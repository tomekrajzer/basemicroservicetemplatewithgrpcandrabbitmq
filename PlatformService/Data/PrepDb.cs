using Microsoft.EntityFrameworkCore;
using PlatformService.Models;

namespace PlatformService.Data
{
    public static class PrepDb
    {
        public static void PrepPopulation(IApplicationBuilder app, bool isProd) 
        {
            using(var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedData(serviceScope.ServiceProvider.GetService<AppDbContext>(), isProd);
            }
        }

        private static void SeedData(AppDbContext context, bool isProd)
        {
            if(isProd)
            {
                Console.WriteLine("--> Attempting to apply migrations...");
                try
                {
                    context.Database.Migrate();
                }
                catch (System.Exception)
                {
                    Console.WriteLine("--> Could not run migrations");    
                }
            }

            if(!context.Platforms.Any())
            {
                Console.WriteLine("--> Seeding Data...");
                context.Platforms.AddRange(
                    new Platform() {Name="Dot net", Publisher="Microsoft", Cost="free"},
                    new Platform() {Name="Sql Server", Publisher="Microsoft", Cost="free"},
                    new Platform() {Name="Kubernetes", Publisher="Native Coimputing Foundation", Cost="free"}
                );

                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("--> We Already have data");
            }
        }
    }
}